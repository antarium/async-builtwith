"""Форк к библиотеке https://bitbucket.org/richardpenman/builtwith.
Предназанчен для определения технологий, используемых сайтами.
Умеет работать как по url, так и с текстом уже скаченной страницы
"""

import os
import re
import json
import pprint
import logging
import chardet
import numpy as np
import asyncio
import aiohttp
from multiprocessing import Process, Queue

class DetectorTachs:
    FILE_TECHS = 'apps.json_old.py'

    def __init__(self):
        self.data = self.load_apps(filename=self.FILE_TECHS)

    def add_app(self, techs, app_name, app_spec):
        """Add this app to technology
        """
        for category in self.get_categories(app_spec):
            if category not in techs:
                techs[category] = []
            if app_name not in techs[category]:
                techs[category].append(app_name)
                implies = app_spec.get('implies', [])
                if not isinstance(implies, list):
                    implies = [implies]
                for app_name in implies:
                    self.add_app(techs, app_name, self.data['apps'][app_name])

    def get_categories(self, app_spec):
        """Return category names for this app_spec
        """
        return [self.data['categories'][str(c_id)] for c_id in app_spec['cats']]

    def contains(self, v, regex):
        """Removes meta data from regex then checks for a regex match
        """
        return re.compile(regex.split('\\;')[0], flags=re.IGNORECASE).search(str(v))

    def contains_dict(self, d1, d2):
        """Takes 2 dictionaries

        Returns True if d1 contains all items in d2"""
        for k2, v2 in d2.items():
            v1 = d1.get(k2)
            if v1:
                if not self.contains(v1, v2):
                    return False
            else:
                return False
        return True

    def load_apps(self, filename='apps.json.py'):
        """Load apps from Wappalyzer JSON (https://github.com/ElbertF/Wappalyzer)
        """
        # get the path of this filename relative to the current script
        # XXX add support to download update
        filename = os.path.join(os.getcwd(), os.path.dirname(__file__), filename)
        return json.load(open(filename))


class BuiltWith(DetectorTachs):
    LOG = "log"
    # максимальное количество одновременно выполняющихся заданий в event loop
    MAX_EVENTS = 20

    def __init__(self):
        super(BuiltWith, self).__init__()
        log_format = "%(asctime)s[LINE:%(lineno)d]#%(levelname)-8s%(message)s"
        logging.basicConfig(filename=self.LOG, level=logging.DEBUG,
                            format=log_format)

    async def fetch(self, url, session):
        """Реализация GET запроса к хосту, для дальнейшего анализа
        также потребуется заголовок ответа и url"""
        try:
            async with session.get(url, ssl=False) as resp:
                status = resp.status
                res_url = resp.url
                headers = resp.headers
                text_raw = await resp.read()
                text = await resp.text()
        except UnicodeDecodeError:
            logging.warning("Decode error")
            status, text, res_url, headers = status, text_raw, res_url, headers
        except Exception as e:
            logging.warning("fetch error: {}".format(e))
            status, text, res_url, headers = None, None, None, None
        return text, status, res_url, headers

    async def file_processing(self, html, res_url, headers):
        """Реализация механизмов определения CMS сайта
        (код оригинала builtwith)"""
        techs = {}
        # check URL
        if res_url:
            for app_name, app_spec in self.data['apps'].items():
                if 'url' in app_spec:
                    if self.contains(res_url, app_spec['url']):
                        self.add_app(techs, app_name, app_spec)
        # check headers
        if headers:
            for app_name, app_spec in self.data['apps'].items():
                if 'headers' in app_spec:
                    if self.contains_dict(headers, app_spec['headers']):
                        self.add_app(techs, app_name, app_spec)
        # check html
        if html:
            for app_name, app_spec in self.data['apps'].items():
                for key in 'html', 'script':
                    snippets = app_spec.get(key, [])
                    if not isinstance(snippets, list):
                        snippets = [snippets]
                    for snippet in snippets:
                        if self.contains(html, snippet):
                            self.add_app(techs, app_name, app_spec)
                            break
            if isinstance(html, bytes):
                coding = chardet.detect(html)["encoding"]
                html = html.decode(coding)
            metas = dict(re.compile(
                '<meta[^>]*?name=[\'"]([^>]*?)[\'"][^>]*?content=[\'"]([^>]*?)[\'"][^>]*?>',
                re.IGNORECASE).findall(html))
            for app_name, app_spec in self.data['apps'].items():
                for name, content in app_spec.get('meta', {}).items():
                    if name in metas:
                        if self.contains(metas[name], content):
                            self.add_app(techs, app_name, app_spec)
                            break
        return techs

    async def read_html(self, path):
        """Метод считывает содержимое файла html и передает его дальше
        обработчику. Возвращает содержимое текста, если удалось прочитать,
        и status (для совместимости с результатом fetch для url"""
        try:
            text = ""
            with open(path, 'r') as file_obj:
                text = file_obj.read()
        except Exception as e:
            logging.warning("readfile error: {}".format(e))
            status = 0
        else:
            status = 200
        return text, status, None, None

    async def survay_html(self, src, val, semaphore, session=None, q_res=None):
        """Функция корутина
        - src принимает два значения "url" или "file" и указыает на источник
        данных для разбора
        - val содержит либо url сайта, либо путь к файлу в зависимости от
        src
        - session aiohttp.Session нужен только при src="url"
        Возвращает словарь с данными, один из ключей которого src_path: val
        - q_res очередь, куда складываем результат (используется только в
        мультипроцессорном режиме)"""
        async with semaphore:
            if src == "url":
                if not val.startswith("http"):
                    val = "http://{}".format(val)
                text, status, res_url, headers = await self.fetch(val, session)
            elif src == "file":
                text, status, res_url, headers = await self.read_html(val)
            if status != 200:
                logging.warning("{} status code: {}".format(val, status))
                result = {"src_path": val}
            else:
                try:
                    result = await self.file_processing(text, res_url, headers)
                    result["src_path"] = val
                except Exception as e:
                    logging.warning("file processing error: {}".format(e))
                    result = {"src_path": val}
                finally:
                    if q_res:
                        q_res.put(result)
        if not q_res:
            return result

    async def create_tasks(self, src, values, loop, q_res=None):
        """отвечает за формирование и запуск корутин, реализующих
        разбор страницы сайта.
        - src  "url" или "file" указыает на источник данных для разбора
        - values может быть строкой или массивом
        - q_res очередь, куда складываем результат (используется только в
        мультипроцессорном режиме)"""
        semaphore = asyncio.Semaphore(value=self.MAX_EVENTS, loop=loop)
        if type(values) == str:
            values = [values]
        if src == "url":
            timeout = aiohttp.ClientTimeout(total=60)
            conn = aiohttp.TCPConnector(ssl=False, limit=self.MAX_EVENTS)
            session = aiohttp.ClientSession(timeout=timeout, connector=conn,
                                            loop=loop)
        else:
            session = None
        coros = [self.survay_html(src, val, semaphore, session, q_res)
                 for val in values]
        done, pending = await asyncio.wait(coros)
        if session:
            await session.close()
        if not q_res:
            res = [item.result() for item in done]
            return res

    def builtwith(self, src, values):
        """Функция разбора
        - src принимает два значения "url" или "file" и указыает на источник
        данных для разбора. Важно: качестве разбора файлов ниже!!!
        - values содержит либо url сайта, либо путь к файлу в зависимости от
        src, может быть как строкой, так и массивом
        """
        loop = asyncio.get_event_loop()
        res = loop.run_until_complete(self.create_tasks(src, values, loop))
        return res

    def builtwith_publisher(self, src, values, q_res):
        """Функционально аналогична методу builtwith, но зфпускает
        свои корутины в отдельном потоке.
        - q_res очередь, куда складываем результат"""
        loop = asyncio.new_event_loop()
        loop.run_until_complete(self.create_tasks(src, values, loop, q_res))
        # закрываем очереди
        res = q_res.put(None)
        return res

    def consumer(self, queue, processes):
        """Обработчик очереди результатоы
        Отвечает за обработку результатов процессов с асинхронной работой
        Completed изменяет свое значение от 0, до кол-ва запущенных
        процессов с асинхронной обработкой, по достижению этого кол-ва
        должна передать сообщение в очередь ошибок о завершении и
        завершить свою работу. На вход из очереди возвращает словарь
        - processes кол-во процессов. Может использоваться для своих
        модификаций, например для запись результатов в какой-то файл или
        в базу данных"""
        completed = 0
        result = []
        while True:
            data = queue.get()
            if data == None:
                completed += 1
            else:
                result.append(data)
            if completed == processes:
                break
        queue.put(result)

    def builtwith_processes(self, src, values, processes=2):
        """Аналогичен методу builtwith, но запускает процесс в нескольких
        независимых процессах"""
        proc_list = []
        q_res = Queue()
        proc = Process(target=self.consumer, args=(q_res, processes))
        proc.daemon = True
        proc.start()
        proc_list.append(proc)
        values = np.array_split(values, processes)
        for i in range(processes):
            proc = Process(
                target=self.builtwith_publisher, args=(src, values[i], q_res))
            proc.daemon = True
            proc.start()
            proc_list.append(proc)
        for proc in proc_list:
            proc.join()
        result = q_res.get()
        q_res.close()
        q_res.join_thread()
        return result


if __name__ == "__main__":
    urls = ["aaahh.ru", "ab-glusgkovru.416.com1.ru", "abikaterm.ru", "ablbur.ru"]
    client = BuiltWith()
    res = client.builtwith("url", urls)
    #res = client.builtwith_processes("url", urls, 2)
    pprint.pprint(res)
